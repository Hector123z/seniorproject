Sprint 4 Planning Sheet : Team Bits Please!
=======================

## As a user, I would like to know what the weather, temperature, and climate would be like in one of these top locations so I can decide if this is a location I would like to potentially travel based on the outside conditions during my stay.
**Adrian Vickers**, **8 Effort Points**

### Description
* Pull specific information from DB into the Controller
* Add data dynamically to the Top Locations Page
* Format the data Appropriately

### Acceptance Criteria
_If a user clicks on one of the Top Locations page, then they should be presented with this additional information per top location as it should load without error._

## As a user, I would like to know the crime rate of the top locations, so I can decide if the location is safe enough for me and my travel companions.
**Adrian Vickers**, **5 Effort Points**

### Description
* Pull this data from the DB into the top locations controller
* Present this information to the user
* Follow formatting as usual

### Acceptance Criteria
_If a user clicks on the Top Locations page, they should be presented with this additional data which should load dynamically and without error._

## As a logged in user, I would like to be able to delete saved favorites on my account so that I can remove the things I don't care about anymore.
**Hector Acosta**, **5 Effort Points**

### Description
* Make each saved favorite in the favorites list of a user have a delete button
* Make the delete button delete the saved favorites
* Will need to access db 

### Acceptance Criteria
_If a user clicks on the delete button of a saved favorite (on some sort of favorites list or other) then it will not be in the favorites list anymore._

## As a user, I would like to be able to easily share my search/trip ideas to social media websites like Facebook, Instagram, or Twitter 
**Tiffany Jansen**, **8 Effort Points**

### Description
* Look up how to get the "Share on Twitter" button.
* Look up how to add the "Share on Facebook" button.
	* etc.
* Put links/buttons in the footer of the site.

### Acceptance Criteria
_If a user presses the button(s), then they will be sent to the social media site._

## As a logged in user I would like to see the result from a search in my search history so that I can remember what kind of information a specific search gave me.
**Hector Acosta**, **13 Effort Points**

### Description
* On search history page make a button(or other) to show the result
* Consider making drop down on each search that can be opened and closed to show/hide result
* Will need to access result of search in db

### Acceptance Criteria
_If a user clicks on a result button(or other means) from a search in the search history then they will see the result of the search._

## As a user, I would like to select the type of flight I want to be on so that I can go first class if I want to.
**Adrian Vickers**, **8 Effort Points**

### Description
* Update the search view page with a new drop down flight type option
* Update the POST Search action method on the controller to require this new input
* Update the API method to include this new data option when determining flight costs
* Include this information to the user on the results page

### Acceptance Criteria
_If a user selects one of the two flight options, then they should be presented with the correct flight averages for that flight type on the search results view page._

## As a user, I would like to be able to customize my accommodation options (i.e. only hotel) so that I don't always have to select both when trying to plan my travel.
**Stacia Fry**, **21 Effort Points**

### Description
* On the search page, add a button so users can select to only search for hotels.
Button takes them to a new view that only searches for hotels and returns their budget break down accordingly. 

### Acceptance Criteria
_If a user visits the search page, they can select "Hotels Only" and search for their hotel budget instead of flight/hotel combo._

## As a user, I would like to be able to select flight options without hotel accommodations (i.e. only flights) so that I can plan without having to select a hotel option.
**Stacia Fry**, **13 Effort Points**

### Description
* On the search page, add a button so users can select to only search for flights.
 Button takes them to a new view that only searches for flights and returns their budget break down accordingly. 

### Acceptance Criteria
_If a user visits the search page, they can select "FlightOnly" and search for their flight budget instead of flight/hotel combo._

## As a user, I would like to see hotel cost per day so that I know how much it would cost per day.
**Hector Acosta**, **5 Effort Points**

### Description
* Change in top favorites page from week to day of hotel costs
* Consider changing table labels to reflect changes
* Add disclaimers about when we queried and how long the stay would be.

### Acceptance Criteria
_If a user clicks on a top location then they will see the hotel costs by star rating calculated for a day with a disclaimer that gives some details of the calculation._

## As a user, I would like the logo to be a little smaller so that the nav-bar isn't so big.
**Stacia Fry**, **2 Effort Points**

### Description
* Resize and recolor the logo on the webpage so it's easier to read and so the navbar is more narrow.

### Acceptance Criteria
_If a user visits our webpage, they will see a crisp logo._

## As a user, I would like to be able to select a country so I can travel to places outside the US.
**Adrian Vickers**, **13 Effort Points**

### Description
* Research countries to add to the DB
* Research cities to add to the DB and create new cities table
* Seed the DB with information
* Find the main airports in these specific cities and add them also to the DB

### Acceptance Criteria
_If a user selects a different country and main city to fly to, they should be presented with the main airport option for said location._

## As a user, I would like to be able to select an "International City" so that I can travel internationally.
**Tiffany Jansen**, **13 Effort Points**

### Description
* Add International Cities to the DB
* Add Javascript so that once the user selects a country they can access the cities
	* Use a dropdown

### Acceptance Criteria
_If a user selects a country besides the US, then they will see a drop down of available cities to fly to._
_If a user selects the US, then the list of states will become the drop down._

## As a user, I would like to be able to select an international airport, so that I can travel internationally.
**Tiffany Jansen**, **13 Effort Points**

### Description
* Allow users to select specific airports
* Once a user selects a city, the airports will show up.
	* Use Javascript
	* Use a dropdown

### Acceptance Criteria
_If a user selects a city, the possible airports will show up._
_If a user selects an airports and hits search, then the search will be made._

## As a logged-in user, I would like to be able to save Locations to my Favorites for easy access in the future.
**Hector Acosta**, **8 Effort Points**

### Description
* Add UserState table (or something like it, edit db in someway).
* Make button to add location to favorites
* button location: location pages?
* Might need to add a way to view favorites (possible temporary location until functionality is added)

### Acceptance Criteria
_If a logged in user clicks on a "save to favorites" button (name can change) then that location will be saved to their favorites list._




